{
    description = "flake for nixos";

    inputs = {
      nixpkgs = {
        url = "github:NixOS/nixpkgs/nixos-unstable";
      };
    };

    outputs = { self, nixpkgs }: {
      nixConfigurations = {
        nixos = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./configuration.nix
          ];
        };
      };
    };
}
